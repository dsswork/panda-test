<?php component('head'); ?>
    <div class="container col-md-6">
<?php component('header'); ?>
    <div class="row justify-content-center">
    <h1><?= $data['pool']['title']; ?></h1>
    <div>
        <table class="table table-striped">
            <thead>
            <tr>
                <th scope="col">Question</th>
                <th scope="col">Votes</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($data['questions'] as $question): ?>
                <tr>
                    <td><?= $question['text'] ?></td>
                    <td><?= $question['votes'] ?></td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <div class="d-flex">
        <form method="post" action="/pools/destroy" class="mx-2">
            <input type="hidden" name="pool_id" value="<?= $data['pool']['id']; ?>">
            <button class="btn btn-danger">Delete Pool</button>
        </form>
        <a class="btn btn-warning mx-2" href="/pools/edit/<?= $data['pool']['id']; ?>">Edit Pool</a>
    </div>

<?php component('footer'); ?>