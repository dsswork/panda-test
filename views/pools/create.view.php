<?php component('head'); ?>

<div class="container  col-md-6">
    <?php component('header'); ?>
    <div class="row justify-content-center">
        <h1>Create pool</h1>
        <form action="/pools/store" method="post">
            <div class="form-group my-2">
                <label for="title">Title</label>
                <input type="text" class="form-control" name="title" id="title" required>
            </div>
            <div class="form-group my-2">
                <label for="title">Answers Number</label>
                <input type="number" class="form-control" min="1"
                       name="questions_number" id="title" required>
            </div>

            <button type="submit" class="btn btn-primary my-2">Create</button>
        </form>

    </div>
</div>
<?php component('footer'); ?>
