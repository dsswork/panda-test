<?php component('head'); ?>
<div class="container col-md-6">
    <?php component('header'); ?>
    <h1>Pools</h1>
    <table class="table table-striped">
        <thead>
        <tr>
            <th scope="col">Title</th>
            <th scope="col">Status</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($data as $pool): ?>
            <tr>
                <td><a href="/pools/show/<?= $pool['id']; ?>"><?= $pool['title']; ?></td>
                <td><?= $pool['is_active'] ? '<b>Active</b>' : 'Disabled' ?></td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
</div>
<?php component('footer'); ?>
