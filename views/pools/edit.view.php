<?php component('head'); ?>
    <div class="container col-md-6">
        <form method="post" action="/pools/update/<?= $data['pool']['id']; ?>" class="mx-2">
            <div class="">
                <?php component('header'); ?>
                <h1>Edit Pool</h1>
                <div class="form-group">
                    <label for="">Title</label>
                    <input class="form-control" name="title"
                           value="<?= $data['pool']['title']; ?>" required>
                </div>
                <div class="form-group">
                    <input type="checkbox" name="is_active" value="0" checked hidden>
                    <input type="checkbox" name="is_active"
                           value="1" <?= $data['pool']['is_active'] ? 'checked' : '' ?>>
                    <label for="">Active Status</label>
                </div>
                <div>
                    <ul>
                        <?php foreach ($data['questions'] as $key => $question): ?>
                        <li>
                            <h3>Answer <?= $key + 1 ?></h3>
                            <input type="hidden"
                                   name="question[<?= $key ?>][id]"
                                   class="form-control" value="<?= $question['id'] ?>">
                            <div class="form-group">
                                <label for="">Text</label>
                                <input type="text"
                                       name="question[<?= $key ?>][text]"
                                       class="form-control" value="<?= $question['text'] ?>"
                                       required
                                >
                            </div>
                            <div class="form-group">
                                <label for="">Votes</label>
                                <input type="text" class="form-control"
                                       name="question[<?= $key ?>][votes]"
                                       value="<?= $question['votes'] ?>"
                                       required
                                >
                            </div>
                            <?php endforeach; ?>
                    </ul>
                </div>

                <div>
                    <button class="btn btn-primary">Save Pool</button>

                </div>
        </form>
    </div>

<?php component('footer'); ?>