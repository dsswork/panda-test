<div class="d-flex my-2">
    <a href="/pools" class="btn btn-dark mx-2">Pools</a>
    <a href="/pools/create" class="btn btn-success mx-2">Create Pool</a>
    <a href="/docs" class="btn btn-primary mx-2">Api Docs</a>
    <form action="/logout" method="post">
        <button class="btn btn-warning mx-2">Logout</button>
    </form>
</div>