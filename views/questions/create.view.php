<?php component('head'); ?>

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-6">
            <h1>Add Questions</h1>
            <form action="/questions/store" method="post">
                <?php for($i=0;$i<$data['number'];$i++): ?>
                    <h2>Question <?= $i+1 ?></h2>
                    <input type="text" name="pool_id" value="<?= $data['poolId']; ?>" hidden>
                <div class="form-group my-2">
                    <label for="title">Question</label>
                    <input type="text" class="form-control" 
                           name="question[<?= $i ?>][title]" id="title" required>
                </div>
                <div class="form-group my-2">
                    <label for="title">Votes</label>
                    <input type="number" class="form-control" min="1" 
                           name="question[<?= $i ?>][vote]" id="title" required>
                </div>
                <?php endfor; ?>    

                <button type="submit" class="btn btn-primary my-2">Create</button>
            </form>
        </div>
    </div>
</div>
<?php component('footer'); ?>
