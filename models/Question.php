<?php

class Question
{
    public static function create($questions, $poolId)
    {
        $sql = "INSERT INTO questions (text, votes, pool_id)
                VALUES "; 

        foreach($questions as $question) {        
            $text = $question['title'];
            $votes = $question['vote'];
            $sql.="('$text', $votes, $poolId),";
        }
        $sql = substr($sql,0,-1);

        pdo()->exec($sql);

        redirect('/pools');
    }

    public static function getAllByPoolId($id) {
        $sql = "SELECT * FROM questions
                WHERE pool_id='$id'";

        $result = pdo()->query($sql);

        return $result->fetchAll();
    }

    public static function massUpdate($questions) {
        $queryText = '';
        $queryVotes = '';
        $ids = '';
        foreach($questions as $question) {
            $queryText .= " WHEN ".$question['id']." THEN '".$question['text']."'";
            $queryVotes .= " WHEN ".$question['id']." THEN ".$question['votes'];
            $ids .= $question['id'].",";
        }
        $ids = substr($ids,0,-1);

        $sql = "UPDATE questions
         SET text = (CASE id ".$queryText." END)
         WHERE id IN (".$ids.")";

        pdo()->exec($sql);

        $sql = "UPDATE questions
         SET votes = (CASE id ".$queryVotes." END)
         WHERE id IN (".$ids.")";

        pdo()->exec($sql);
    }
}