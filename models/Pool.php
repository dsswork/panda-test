<?php
class Pool
{
    public static function all()
    {
        $userId = $_SESSION['user_id'];
        $sql = "SELECT * FROM pools WHERE user_id='$userId'";
        $result = pdo()->query($sql);

        $pools = $result->fetchAll();
        return $pools;
    }

    public static function create($title)
    {
        $userId = $_SESSION['user_id'];
        $sql = "INSERT INTO pools(title, user_id)
                VALUES ('$title', $userId)";

        pdo()->exec($sql);

        $sql = "SELECT * FROM pools ORDER BY id DESC LIMIT 1";

        $pool = pdo()->query($sql)->fetch();

        return $pool;
    }

    public static function destroy($id)
    {
        $sql = "DELETE FROM pools 
                WHERE id = '$id'";

        pdo()->exec($sql);

        $sql = "DELETE FROM questions 
        WHERE pool_id = '$id'";
        pdo()->exec($sql);
    }

    public static function Update($id, $title, $isActive) {
        $sql = "UPDATE pools
                SET title = '$title', is_active = '$isActive'
                WHERE id='$id'";

        pdo()->exec($sql);
    }

    public static function getPool($id)
    {
        $sql = "SELECT * FROM pools
                WHERE id ='$id'";

        $result = pdo()->query($sql);

        return $result->fetch();
    }
}