<?php

return [
         'random' => 'ApiController/random',
         'docs' => 'DocController/index',
         'index' => 'SiteController/index',
         'questions/store' => 'QuestionController/store',
         'login/store' => 'LoginController/store',
         'login' => 'LoginController/index',
         'logout' => 'LoginController/destroy',
         'registration/store' => 'RegisterController/store',
         'registration' => 'RegisterController/create',
         'pools/show' => 'PoolController/show',
         'pools/edit' => 'PoolController/edit',
         'pools/create' => 'PoolController/create',
         'pools/store' => 'PoolController/store',
         'pools/destroy' => 'PoolController/destroy',
         'pools/update' => 'PoolController/update',
         'pools' => 'PoolController/index',
];