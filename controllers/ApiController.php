<?php
require_once('models/Question.php');

class ApiController
{
    public function random()
    {
        $userId = $_SESSION['user_id'];

        $sql = "SELECT * FROM pools
                WHERE user_id='$userId' AND is_active=1
                ORDER BY RAND()
                LIMIT 1";

        $result = pdo()->query($sql);

        $pool = $result->fetch();
        if($pool) {

            $questions = Question::getAllByPoolId($pool['id']);

            $resultArray = ['title' => $pool['title']];

            foreach ($questions as $key => $question) {
                $resultArray['questions'][$key]['text'] = $question['text'];
                $resultArray['questions'][$key]['votes'] = $question['votes'];
            }
        } else {
            $resultArray = ['status' => 'You dont have active pools'];
            http_response_code(206);
        }
        header("Content-Type: application/json");
        echo(json_encode($resultArray));
    }
}