<?php

class SiteController
{
    public function index()
    {
        if (check_auth()) {
            redirect('/pools');
        } else {
            redirect('/login');
        }
    }
}