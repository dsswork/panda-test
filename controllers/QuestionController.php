<?php
require_once('models/Question.php');


class QuestionController
{
    public function store()
    {
        auth_guard();
        $questions = $_POST['question'];
        $poolId = $_POST['pool_id'];
        Question::create($questions, $poolId);
    }
}