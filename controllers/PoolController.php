<?php
require_once('models/Pool.php');
require_once('models/Question.php');


class PoolController
{
    public function index()
    {
        auth_guard();
        $pools = Pool::all();
        view('pools/index', $pools);
    }

    public function create()
    {
        auth_guard();
        view('pools/create');
    }

    public function store()
    {
        auth_guard();
        $title = htmlspecialchars($_POST['title']);
        $number = $_POST['questions_number'];
        $pool = Pool::create($title);
        $poolId = $pool['id'];
        $data = compact('number', 'poolId');

        view('questions/create', $data);
    }

    public function show($id)
    {
        auth_guard();
        $pool = Pool::getPool($id);
        $questions = Question::getAllByPoolId($id);

        view('pools/show', compact('pool', 'questions'));
    }

    public function edit($id)
    {
        auth_guard();
        $pool = Pool::getPool($id);
        $questions = Question::getAllByPoolId($id);

        view('pools/edit', compact('pool', 'questions'));
    }

    public function update($id)
    {
        auth_guard();

        $title = $_POST['title'];
        $isActive = $_POST['is_active'];
        Pool::update($id, $title, $isActive);

        $questions = $_POST['question'];
        Question::massUpdate($questions);

        $pool = Pool::getPool($id);
        $questions = Question::getAllByPoolId($id);

        view('pools/show', compact('pool', 'questions'));
    }

    public function destroy()
    {
        auth_guard();
        $id = $_POST['pool_id'];
        Pool::destroy($id);
        redirect('/pools');
    }
}