<?php

class DocController {
    public function index() {
        auth_guard();
        view('docs');
    }
}